// Get npm modules
const express = require('express');
const { createServer } = require('http');
const { Server } = require('socket.io');
const ip = require('ip');
// --
const path = require('path');
//const colors = require('colors');

class Wss {
  static init(opts = {}) {
    // Set @params
    this.port = opts.port || 8080;
    this.publicDir = opts.publicDir || 'public';

    // Init the server
    this.app = express();
    this.httpServer = createServer(this.app);
    this.io = new Server(this.httpServer, {});

    // Link files to be served
    this.app.get('/', (req, res) => {
      res.setHeader('Content-Type', 'text/html');
      res.sendFile(path.join(this.publicDir, 'index.html'));
    });

    // Set the port to listen the server
    this.httpServer.listen(this.port, () => {
      this._info('listening on http://' + ip.address() + ':' + this.port);
    });

    // Set a listener for sockets
    this.sockets = this.io.listen(this.httpServer).sockets;
  }

  // What happen on new user
  static newUser(socket) {
    const addr = socket.address.slice(7);
    // Print that a new connection has been established
    this._info(`new connection from ${addr} (${this.io.engine.clientsCount})`);
  }

  // What happen on lost user
  static lostUser(socket) {
    const addr = socket.address.slice(7);
    // Print that a new connection has been established
    this._info(`lost connection from ${addr} (${this.io.engine.clientsCount})`);
  }

  /**
   * Private methods
   */
  static _info(msg) {
    console.log(`app: wss ⇨ ${msg}`);
  }
}

module.exports = Wss;

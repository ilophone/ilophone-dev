const fs = require('fs');
const fse = require('fs-extra');
const path = require('path');
const colors = require('colors');

class Dolph {
  static async init(opts = {}) {
    // Init currentData obj
    this.currentData = new Object();
    // Get app directory
    this.appDir = path.dirname(require.main.filename);
    // Get app/core directory
    this.coreDir = path.join(this.appDir, 'core');
    // Check for the user directory
    if (opts.userDir == 'default' || opts.userDir == '') {
      this.userDir = path.join(process.env.HOME, 'Documents/Ilophone');
      fs.access(this.userDir, (err) => {
        if (err) {
          fs.mkdir(this.userDir, { recursive: true }, (err) => {
            if (err) throw err;
          });
          fse.copy(path.join(this.appDir, 'user'), this.userDir, function (err) {
            if (err) throw err;
          });
        }
      });
    } else this.userDir = opts.userDir;
    // Check for the sessionData file to exists
    this.sessionDataFile = path.join(this.userDir, 'sessions', `${opts.sessionDataFile}.json`);
    try {
      this.sessionData = require(this.sessionDataFile);
    } catch (err) {
      this.sessionData = new Object();
    }
  }

  // Save given preset
  static savePreset(data) {
    // Split data
    data = data.split(' ');
    // Extract datas
    const modToSave = data[0];
    const presetNum = data[1];
    // Deduce the preset filename
    const presetFile = this.sessionData.module[modToSave].canvasname;
    const presetFileName = path.join(this.userDir, 'presets', `${presetFile}.json`);
    // Load existing datas in the preset object
    var preset;
    try { // if module exists
      preset = require(presetFileName);
    } catch (err) { // if not
      preset = new Array(8);
    }
    // Load current datas in the dedicated preset slot
    try {
      preset[presetNum] = this.currentData.module[modToSave];
    } catch (err) {
      // This could not happen because all Pd controls are 'bangged' on startup
      console.log(`app: warning: currentData.module[${modToSave}] returns null`);
      console.log('app: preset could not be saved');
      return;
    }
    // Write the js obj in a json file
    fs.writeFile(presetFileName, JSON.stringify(preset, null, 2), err => {
      if (err) return console.log(err);
      else console.log(`app: ${presetFile}-${presetNum} preset has been saved`);
    });
    // Release the loaded cache from the preset file
    delete require.cache[require.resolve(presetFileName)];
  }

  // Load given preset
  static loadPreset(data, Pd) {
    // Split data
    data = data.split(' ');
    // Extract datas
    const modToLoad = data[0];
    const presetNum = data[1];
    // Deduce the preset filename
    const presetFile = this.sessionData.module[modToLoad].canvasname;
    const presetFileName = path.join(this.userDir, 'presets', `${presetFile}.json`);
    // Load datas in the preset obj
    var preset;
    try { // if module exists
      preset = require(presetFileName);
    } catch (err) { // if not
      console.log(`app: warning: there is currently no preset file for the ${presetFile} module`);
      console.log('app: please save a preset first');
      return;
    }
    // Load datas in an object
    const obj = preset[presetNum];
    // Ensure the given object is not null
    if (obj === null) {
      console.log('app: warning: obj returns null');
      console.log('app: preset is empty so could not be loaded');
      return;
    }
    // Prepare loaded datas for Pd
    const dumpArray = this.jsonToOsc(obj);
    // For each data that contains the array
    for (var i = 0; i < Object.keys(dumpArray).length; i++) {
      // Extract datas
      const tag = Object.keys(dumpArray)[i];
      const msg = Object.values(dumpArray)[i];
      // Send each addr + data pair to Pd
      Pd.send(`/module/${modToLoad}/${tag}`, `${msg}`);
    }
    console.log(`app: ${presetFile}-${presetNum} preset has been loaded`.grey);
    // Update the loaded preset in sessionData
    this.sessionData.module[modToLoad].preset = presetNum;
    // Release the loaded cache from the preset file
    delete require.cache[require.resolve(presetFileName)];
  }

  // Session save
  static sessionSave(Pd) {
    const mainPatchName = '__main';

    console.log('app: saving session ...');

    Pd.send(`pd-${mainPatchName}.pd`, 'menusave');
    Pd.send(`pd-${mainPatchName}.pd`, 'editmode 0');

    // Ping the module
    // Are you still here ?
    this.sessionData.module.forEach((_module, _moduleIndex) => {
      if (_module !== null)
      {
        Pd.send(`/module/${_moduleIndex}/ping`, '0');
      }
    });

    // Wait for dump to be recived
    // and write the session file from the sessionData obj
    setTimeout(() => {
      this.sessionData.module.forEach((_module, _moduleIndex) => {
        if (_module !== null)
        {
          if (_module.pong !== 'here') this.sessionData.module[_moduleIndex] = null;
          else delete _module.pong;
        }
      });

      fs.writeFile(this.sessionDataFile, JSON.stringify(this.sessionData, null, 2), err => {
        if (err) return console.log(err);
        console.log(`app: session saved to ${this.sessionDataFile}`);
      });
    }, 1000);
  }

  // Build canvas
  static buildCanvas(Pd) {
    this.sessionData.module.forEach((_module, _moduleIndex) => {
      if (_module !== null)
      {
        /*
         *  Trying to build the main patch from session file
         */
        const canvasname = _module.canvasname;

        const canvasposx = _module.cnvpos[0];
        const canvasposy = _module.cnvpos[1];

        // Create objects
        Pd.send('pd-__main.pd',
                `obj ${canvasposx} ${canvasposy} ${canvasname} ${_moduleIndex}`);

        // Init the module
        // loadbang is not working here ...
        Pd.send(`/modules/init`, '0');

        // Init sliders
        // loadbang is not working here ...
        Pd.send(`/module/${_moduleIndex}/sliders/init`, '0');
      }
    });
  }

  // Session load
  static sessionLoad(Pd) {
    this.sessionData.module.forEach((_module, _moduleIndex) => {
      if (_module !== null)
      {
        if (typeof _module.slider !== 'undefined') {
          const dumpArray = this.jsonToOsc(_module.slider);
          for (var i = 0; i < Object.keys(dumpArray).length; i++) {
            // Extract datas
            const tag = Object.keys(dumpArray)[i];
            const msg = Object.values(dumpArray)[i];
            // ==> Wss send on dump
          }
        }

        if (typeof _module.preset !== 'undefined' ) {
          Pd.send(`/module/${_moduleIndex}/preset`, `${_module.preset}`);
        }
      }
    });
  }

  // Remove null items
  // from the given object
  static removeEmpty(obj) {
    return Object.fromEntries(
      Object.entries(obj).filter(([_, v]) => v != null)
    );
  }

  // Return an array filled by
  // pairs of keys and values from the given
  // obj in an OSC style (e.g. /slider/1/value + 127)
  static jsonToOsc(obj, prefix) {
    obj = this.removeEmpty(obj);
    var arr = [];
    for (var k of Object.keys(obj)) {
      var val = obj[k],
        key = prefix ? prefix + '/' + k : k;
      if (typeof val === 'object')
        Object.assign(arr, this.jsonToOsc(val, key));
      else
        arr[key] = val;
    }
    return arr;
  }

  // Fill a given object with an
  // OSC pattern message : addr + msg
  static oscToJson(data, obj) {
    // Split data
    data = data.split(' ');
    // Extract datas
    var tag = data[0]; // address
    var msg = data[1]; // message
    // Prepare datas
    tag = tag.split('/');
    tag.shift();
    // Set the given object
    setObject(obj, tag, msg);
  }
}

// If given string
// contains a number
function hasNumber(myString) {
  return /\d/.test(myString);
}

// Fill a given object with:
// - keys in an array
// - an associate value
function setObject(obj, prop, val) {
  prop.forEach(function(el, index, array) {
    if (index === array.length - 1) {
      obj[el] = val;
    } else {
      if (hasNumber(array[index + 1])) obj[el] = obj[el] || [];
      else obj[el] = obj[el] || {};
      obj = obj[el];
    }
  });
}

module.exports = Dolph;

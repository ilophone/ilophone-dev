// Get npm modules
const { exec, spawn } = require('child_process');
const fs = require('fs');
const path = require('path');
//const colors = require('colors');

// Get patch module from src
const Patch = require('./patch.js');

class Pd {
  static async init(Dolph, opts = {}) {
    // Set @params
    this.dolph = Dolph;

    this.binFolder = '/usr/bin';
    this.tmpDir = path.join(this.dolph.appDir, '.tmp');

    this.pdFlags = opts.pdFlags || [];
    this.pdsendPort = opts.pdsendPort || 8888;
    this.pdreceivePort = opts.pdreceivePort || 9999;

    this.pd = null;
    this.pdsend = null;
    this.pdreceive = null;

    this.patchConfig = {
      pdreceivePort: this.pdreceivePort,
      pdsendPort: this.pdsendPort
    }

    this.patch = new Patch(this.patchConfig);

    this.hasBeenInit = false;

    // Check if tmp dir exists
    if (!fs.existsSync(this.tmpDir)) {
      // Make tmp dir
      fs.mkdirSync(this.tmpDir);
    }

    // Read content of tmp dir
    const files = fs.readdirSync(this.tmpDir);

    files.forEach((file) => {
      // Remove files in tmp dir if exists
      fs.unlinkSync(path.join(this.tmpDir, file));
    });

    await this.start();
  }

  // Start processes
  static async start() {
    return new Promise(async (resolve, reject) => {
      await this._startPdReceive()
      .then(() => {
        this._startPdSend();
        this.send('pd-netsend.pd', 'vis 0');
        this.send('pd-netreceive.pd', 'vis 0');
        resolve();
      }, async () => {
        await this._kill('pdsend', 'SIGTERM');
        await this._kill('pd', 'SIGTERM');
        await this._kill('pdreceive', 'SIGTERM');

        await this._sleep(5000);
        await this.start();
        resolve();
      });
    });
  }

  // Start Pd process
  static _startPd() {
    const receivePatchPath = this._createPdPatch('netreceive', this.patch.netreceive);
    const sendPatchPath = this._createPdPatch('netsend', this.patch.netsend);
    const mainPatchPath = this._createPdPatch('__main', this.patch.main);

    const options = this.pdFlags.concat([
      '-stderr',
      '-rt',
      '-path', `${path.join(this.dolph.userDir, 'modules')}`,
      '-path', `${path.join(this.dolph.coreDir, 'lib')}`,
      '-open', `${receivePatchPath}`,
      '-open', `${sendPatchPath}`
    ]);

    this.pd = spawn(path.join(this.binFolder, 'pd'), options);

    // print console output (yes, pd's console output goes to stderr !)
    // this.pd.stderr.on('data', (data) => {
    //   // this handler is now managed in the app
    // });

    this.pd.on('exit', function(code) {
      // Clear temp files
      // this.clear();
      // Exit node
      console.log('pd : pd has been closed');
      process.exit();
    });
  }

  // Start Pdsend process
  static _startPdSend() {
    this.pdsend = spawn(path.join(this.binFolder, 'pdsend'), [`${this.pdsendPort}`, 'localhost', 'udp']);

    // this.pd.stdout.on('data', (data) => {
    //   console.log(`pdsend stdout : ${data}`.slice(0, -1));
    // });
    //
    // this.pd.stderr.on('data', (data) => {
    //   console.log(`pdsend stderr : ${data}`.slice(0, -1));
    // });

    this.pdsend.on('exit', function(code) {
      // Clear temp files
      // this.clear();
      // Exit node
      console.log('pd : pdsend has been closed');
      process.exit();
    });
  }

  // Start Pdreceive process
  static async _startPdReceive() {
    return new Promise((resolve, reject) => {
      this.pdreceive = spawn(path.join(this.binFolder, 'pdreceive'), [`${this.pdreceivePort}`, 'udp']);

      // Wait for initialized to resolve
      this.pdreceive.stdout.on('data', (data) => {
        if (`${data}` === 'initialized;\n') {
          this.hasBeenInit = true;
          resolve();
        }
      });

      // Print Pdreceive stderr output
      this.pdreceive.stderr.on('data', (data) => {
        console.log(`pdR: ${data}`.slice(0, -1));
        reject();
      });

      this.pdreceive.on('exit', function(code) {
        // Clear temp files
        // this.clear();
        // Exit node
        console.log('pd : pdreceive has been closed');
        process.exit();
      });

      this._startPd();
    });
  }

  // Send a msg to Pd
  static send(tag, msg) {
    this.pdsend.stdin.write(`${msg} ${tag};\n`);
  }

  // Open a pd patch
  static open(patchPath) {
    const filedir = path.dirname(patchPath);
    const filename = path.basename(patchPath);
    this.pdsend.stdin.write(`open ${filename}.pd ${filedir} pd;\n`);
  }

  // Clear tmp dir
  static clear() {
    fs.rmdirSync(this.tmpDir, { recursive: true });
  }

  // Create a patch from a template
  static _createPdPatch(id, contents) {
    const patchPath = path.join(this.tmpDir, `${id}.pd`);
    fs.writeFileSync(patchPath, contents);
    return patchPath;
  }

  static test() {
    // Create a new canvas with name test
    this.send('pd', `menunew test ${this.tmpDir}`);
    // Create new objects
    this.send('pd-test', 'text 33 26 [ Test Pd patch file ]');
    this.send('pd-test', 'obj 33 70 osc~ 440');
    this.send('pd-test', 'obj 33 100 *~ 0.1');
    this.send('pd-test', 'obj 33 130 dac~ 1 2');
    // Connect obj to each others
    this.send('pd-test', 'connect 1 0 2 0');
    this.send('pd-test', 'connect 2 0 3 0');
    this.send('pd-test', 'connect 2 0 3 1');
  }

  // Kill a process
  static async _kill(procName, signal) {
    return new Promise((resolve, reject) => {
      const e = exec(`killall -s ${signal} ${procName}`);
      e.on('exit', () => {
        resolve();
      });
    });
  }

  // Sleep for a duration (ms)
  static async _sleep(duration) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, duration);
    });
  }
}

module.exports = Pd;

class Patch {
  constructor(opts = {}) {

    /**
     * Patch needed variables
     */
    this.pdreceivePort = opts.pdreceivePort || 9999;
    this.pdsendPort = opts.pdsendPort || 8888;

    /**
     * Pd send patch template
     */
    this.netsend = `
    #N canvas 1327 497 392 369 10;
    #X obj 228 331 netsend -u;
    #X msg 228 169 connect localhost ${this.pdreceivePort};
    #X obj 201 41 t b b;
    #X obj 201 18 loadbang;
    #X obj 105 169 list prepend send;
    #X obj 105 192 list trim;
    #X obj 105 19 r fromPd;
    #X msg 143 115 initialized;
    #X obj 20 19 r pd;
    #X obj 20 42 list prepend pd, f 7;
    #X connect 1 0 0 0;
    #X connect 2 0 7 0;
    #X connect 2 1 1 0;
    #X connect 3 0 2 0;
    #X connect 4 0 5 0;
    #X connect 5 0 0 0;
    #X connect 6 0 4 0;
    #X connect 7 0 4 0;
    #X connect 8 0 9 0;
    #X connect 9 0 4 0;
    `;

    /**
     * Pd receive patch template
     */
    this.netreceive = `
    #N canvas 1033 382 392 172 10;
    #X obj 74 16 netreceive ${this.pdsendPort} 1;
    #X obj 74 39 list length;
    #X obj 74 62 - 1;
    #X obj 17 89 list split;
    #X obj 17 112 list trim;
    #X obj 17 135 s;
    #X connect 0 0 1 0;
    #X connect 0 0 3 0;
    #X connect 1 0 2 0;
    #X connect 2 0 3 1;
    #X connect 3 0 4 0;
    #X connect 3 1 5 1;
    #X connect 4 0 5 0;
    `;

    /**
     * Main patch template
     */
    this.main = `
    #N canvas 515 376 681 592 10;
    #X text 104 31 [ Main patch file ];
    #X obj 33 31 MAIN_INIT;
    `;
  }
}

module.exports = Patch;

/**
 * TO-DO-LIST :
 * ============
 * - Audio routing ==> dynamic patching cf. Matrix
 * - Session saving
 * - Update main.pd patch
 * - Clean
 */


// *****************************************************************************
//                                 MODULES
// *****************************************************************************


// Get npm modules
const fs = require('fs');
const path = require('path');
//const colors = require('colors/safe');
const colors = require('colors');

// Get Dolphin from modules
const Dolph = require('../modules/dolphin');

// Get Wss from modules
const Wss = require('../modules/wss');

// Get Pd module from modules
const Pd = require('../modules/wrapper');

// Get app parameters
const manifest = require('./manifest.json');


// *****************************************************************************
//                              CONFIGURATION
// *****************************************************************************

// Dolph parameters
const dolphConfig = {
  userDir: path.join(manifest.dolph.userDir),
  sessionDataFile: manifest.dolph.sessionDataFile
}

// Wss parameters
const wssConfig = {
  port: manifest.wss.port,
  publicDir: path.join(__dirname, `${manifest.wss.publicDir}`)
};

// Pd parameters
const pdConfig = {
  pdFlags: manifest.wrapper.pdFlags,
  pdsendPort: manifest.wrapper.pdsendPort,
  pdreceivePort: manifest.wrapper.pdreceivePort
};


// *****************************************************************************
//                                 DOLPH-INIT
// *****************************************************************************


Dolph.init(dolphConfig);


// *****************************************************************************
//                             WEBSOCKET-INIT
// *****************************************************************************


// Apply @params
Wss.init(wssConfig);

// Set what happen when
// a new connection is established
Wss.io.on('connection', (socket) => {
  // Print out infos
  Wss.newUser(socket.handshake);

  /**
   *  Dump initial data ...
   */

  // Say hello on connection
  socket.emit('console', 'wss: connected');

  

  /**
   *  Set handlers
   */

  // Testing handler
  // socket.on('test', (data) => {
  //   console.log('a test has been received');
  // });

  // Example
  // socket.on('my other event', (data) => {
  //   console.log('ws: ' + data);
  // });

  // Set what happen when
  // a socket connection is lost
  socket.on('disconnect', () => {
    // Print out infos
    Wss.lostUser(socket.handshake);
  });
});

// Link files to be served ()
// Wss.app.get('/console', (req, res) => {
//   res.sendFile(path.join(Wss.publicDir, 'console', 'index.html'));
// });

Wss.app.get('/index.js', function(req, res) {
  res.setHeader('Content-Type', 'text/javascript');
  res.sendFile(path.join(Wss.publicDir, 'js', 'index.js'));
});

Wss.app.get('/index.css', function(req, res) {
  res.setHeader('Content-Type', 'text/css');
  res.sendFile(path.join(Wss.publicDir, 'css', 'index.css'));
});

Wss.app.get('/knob.png', function(req, res) {
  res.sendFile(path.join(Wss.publicDir, 'media', 'knob.png'));
});

Wss.app.get('/toggle-on.png', function(req, res) {
  res.sendFile(path.join(Wss.publicDir, 'media', 'toggle-on.png'));
});

Wss.app.get('/toggle-off.png', function(req, res) {
  res.sendFile(path.join(Wss.publicDir, 'media', 'toggle-off.png'));
});


// *****************************************************************************
//                                  PD-INIT
// *****************************************************************************



// Apply @params
Pd.init(Dolph, pdConfig).then(() => {
  // Print out that Pd is ready
  console.log('app: wrapper ⇨ ready');

  /**
   *  When Pd is launched ...
   */

  // Write something
  // to be delayed here
  setTimeout(() => {
    // Open the test patch
    //Pd.test();
    // Open the main patch
    Pd.open(path.join(Dolph.userDir, manifest.dolph.mainPdPatch));
    //Pd.open(path.join(Pd.tmpDir, '__main'));
    // delay (ms)
  }, 1000); // <== delay (ms)

  setTimeout(() => {
    // Load the session
    //Dolph.buildCanvas(Pd);
    // delay (ms)
  }, 2000);


  setTimeout(() => {
    // Load the session
    Dolph.sessionLoad(Pd);
    console.log('app: session should be loaded');
    // delay (ms)
  }, 3000);


  setTimeout(() => {
    // Set Pd's DSP ON
    //Pd.send('pd', 'dsp 1'); /// <==
    // delay (ms)
  }, 4000);

  // setTimeout(() => {
  //   // delay (ms)
  // }, 4000);
});

// *****************************************************************************
//                                 Bridge
// *****************************************************************************


// Print Pd's output on the web-console
Pd.pd.stderr.on('data', (data) => {
  // Remove line breaks
  data = `${data}`.split('\n').slice(0, -1);
  // Check if there's several outputs
  // and for each of them ...
  for (var i = 0; i < data.length; i++) {
    // Split data in words
    data[i] = data[i].split(' ');
    // Remove 'pd:' redundancy
    if (data[i][0] == 'Pd:') {
      data[i].shift();
    }
    // Parse tag and msg from data
    const tag = data[i].shift();
    const msg = data[i].join(' ');

    if (tag == 'verbose(3):')
      return;

    if (tag == 'verbose(3):')
      return;

    // Print and emit
    console.log(`pd : ${tag} ${msg}`.grey);
    Wss.sockets.emit('console', `pd : ${tag} ${msg}`);
  }
});

// Print Pdreceive output on the web-console
Pd.pdreceive.stdout.on('data', (data) => {
  // If Pd has been init
  if (Pd.hasBeenInit) {
    // Remove line breaks and store
    // pdreceive outputs in the data array
    data = `${data}`.replace(/\n/g, '').split(';')
    // Check if there's several outputs
    // and for each of them ...
    for (var i = 0; i < data.length; i++) {
      // Split data in words
      data[i] = data[i].split(' ');
      // Parse tag and msg from data
      const tag = data[i].shift();
      const msg = data[i].join(' ');
      // Instructions per tag
      switch (tag) {
        // Console
        case 'console':
          Wss.sockets.emit('console', `pdR: ${msg}`);
          break;
        // Core
        case 'core':
          Dolph.oscToJson(msg, Dolph.currentData);
          break;
        // Session
        case 'session':
          Dolph.oscToJson(msg, Dolph.sessionData);
          break;
        // Save preset
        case 'savePreset':
          Dolph.savePreset(msg);
          break;
        // Load preset
        case 'loadPreset':
          Dolph.loadPreset(msg, Pd);
          break;
        // Debug core ~
        case 'sessionSave':
          Dolph.sessionSave(Pd);
          break;
        case 'sessionLoad':
          Dolph.sessionLoad(Pd);
        // Pd
        case 'pd':
          // Split data in words
          data[i] = msg.split(' ');
          // Parse tag and msg from data
          const pd_tag = data[i].shift();
          const pd_msg = data[i].join(' ');
          // Instr. per msg
          switch (pd_tag) {
            // dsp
            case 'dsp':
              if(pd_msg == '1') {
                console.log('app: DSP is running');
                Wss.sockets.emit('console', 'app: DSP is running');
              }
              else {
                console.log('app: DSP was turned off');
                Wss.sockets.emit('console', 'app: DSP was turned off');
              }
              break;
            // Ping
            case 'ping':
              // indicator ?
              break;
            // Watchdog
            case 'watchdog':
              // indicator ?
              break;
            // Key
            case 'key':
              // indicator ?
              break;
            // Others
            default:
              console.log(`pdR: ${pd_tag} ${pd_msg}`.grey);
              Wss.sockets.emit('console', `pdR: ${pd_tag} ${pd_msg}`);
              break;
          }
          break;
          // Others
        default:
          Wss.sockets.emit(`${tag}`, `${msg}`);
          break;
      }
    }
  }
});


// *****************************************************************************
//                              Process (node)
// *****************************************************************************

process.stdin.on('data', function(data) {
  // Split data and remove line breaks
  data = data.toString().replace(/\n/g, '').split(' ');
  // Extract datas
  var tag = data.shift();
  var msg = data.join(' ');
  // Send msg to Pd
  Pd.send(tag, msg);
});

// What to do on CTRL-C ?
process.on('SIGINT', () => {
  // Print that a SIGINT signal has been received
  // console.log('received CTRL-C');
  // Send a last msg to sockets ?
  // Wss.sockets.emit('console', 'ws : server has been shutdown');
  // Last save
  //Dolph.sessionSave(); <-- Auto-save on quit ?? Erase the sessionData file ...
  // Clear temp files
  Pd.clear();
  // Exit node
  console.log('app: exiting process');
  process.exit();
});

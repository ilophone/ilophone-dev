// jquery like
$ = document.getElementById.bind(document);

// Setup
var socket = io();
//var channel = 0;

// Socket handler
socket.on('console', function(event) {
  // Debug
  console.log(event);

  parseData(event);
});

function parseData(data) {
  var data = `${data}`.split(' '); data.shift();
  var addr = data[0].split('/'); addr.shift();

  //console.log(data);

  if (addr[0] == 'module') {
    $('channelNum').innerHTML = addr[1];
  }

  if (addr[2] == 'slider') {
    var sliderValue = data[2];
    var angle = sliderValue / 100 * 270;

    $('knob_' + addr[3]).querySelector('#value').innerHTML = '<p>' + sliderValue + '</p>';
    $('knob_' + addr[3]).querySelector('#picture').style.transform = 'rotate(' + angle + 'deg)';
    $('knob_' + addr[3]).querySelector('#name').innerHTML = '<p>' + data[1] + '</p>';
  }

  if (addr[2] == 'toggle') {
    var toggleValue = data[2];

    if (toggleValue == 1) {
      $('toggle_' + addr[3]).querySelector('#picture').src = "toggle-on.png";
    }

    else {
      $('toggle_' + addr[3]).querySelector('#picture').src = "toggle-off.png";
    }

    $('toggle_' + addr[3]).querySelector('#name').innerHTML = '<p>' + data[1] + '</p>';
  }
}

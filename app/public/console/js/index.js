// jquery like
$ = document.getElementById.bind(document);

// Setup
var socket = io();

$("mainFrame").style.display = "block";

// Socket handler
socket.on('console', function(event) {
  //const data = JSON.parse(event);
  log(event);
  console.log(event);
});

// Adds entry to the html #console
function log(txt) {
  var el = document.querySelector('#consoleContainer');
  var newLine = document.createElement("li");
  newLine.innerHTML = (typeof txt === 'string') ? txt : JSON.stringify(txt, null, 4);
  //el.appendChild(newLine);
  el.insertBefore(newLine, el.childNodes[0] || null);
}

// function showDiv(id) {
//   var el = document.getElementById(id);
//   if (el.style.display === "none") {
//     el.style.display = "block";
//   } else {
//     //el.style.display = "none";
//   }
// }

function show(elements, specifiedDisplay) {
  elements = elements.length ? elements : [elements];
  for (var index = 0; index < elements.length; index++) {
    elements[index].style.display = specifiedDisplay || 'block';
  }
}

function hide(elements) {
  elements = elements.length ? elements : [elements];
  for (var index = 0; index < elements.length; index++) {
    elements[index].style.display = 'none';
  }
}

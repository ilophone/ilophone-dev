# Ilophone-dev [ work in progress ]

Made and tested on raspberrypi/linux. A dedicated OS can be download [here](https://ilophone.woollystud.io/downloads/os/).

## Install (linux)

1. Clone this repository :

```sh
git clone https://gitlab.com/ilophone/ilophone-dev.git --depth 1
cd ilophone-dev/
```

2. Install [node / npm](https://nodejs.org/en/download/current/) (if you don't already have them) and other requirements :

```sh
cat requirements.txt | xargs sudo apt install -y
```

3. Install modules :

```sh
npm install
```

4. Run examples :

```sh
npm run wss
npm run wrapper
```

5. Run the app

```sh
./ilophone-app
```

You should see some activity in your terminal (and puredata should pop on your screen) :

![](./screenshots/launch.png)

You can edit the manifest.json file to update the app configuration :

![](./screenshots/manifest.png)

Click on this link to monitor Pd's activity in your prefered web browser :

[https://raspberrypi.local:8080](https://raspberrypi.local:8080)
or type https://your_ip_addr:8080 directly in the search bar.

I suggest to use chromium and private navigation.

![](./screenshots/pd-console.png)

#!/bin/bash
#
#

# Update file tracking related to .gitignore files

cd $(dirname $0)

GIT_DIR=..

cd $GIT_DIR

git rm -r --cached .
git add .
git commit -m "Untracked files issue resolved to fix .gitignore"
git push origin

exit 0

const path = require('path');

// Get Wss from src
const Wss = require('../../modules/wss');

// Get app parameters
const manifest = require('./manifest.json');

// Clear console ?
//process.stdout.write('\033c');

//Wss parameters
const wssConfig = {
  port: manifest.wss.port,
  publicDir: path.join(__dirname, `${manifest.wss.publicDir}`)
};

Wss.init(wssConfig);

Wss.io.on('connection', (socket) => {
  // Print out infos
  Wss.newUser(socket.handshake);

  // Dump on connection
  socket.emit('console', 'hello new user :)');

  // Testing handler
  socket.on('test', (data) => {
    console.log('a test has been received');
  });

  // Example
  // socket.on('my other event', (data) => {
  //   console.log('ws: ' + data);
  // });

  socket.on('disconnect', () => {
    // Print out infos
    Wss.lostUser(socket.handshake);
  });
});

// Emitting example
// setTimeout(() => {
//   Wss.sockets.emit('console', 'testing'); // <== To every sockets
//   console.log('a test has been sent');
// }, 5000);

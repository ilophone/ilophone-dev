const colors = require('colors/safe');
const path = require('path');

// Get Pd module from src
const Pd = require('../../modules/wrapper');

// Get app parameters
const manifest = require('./manifest.json')

// Pd parameters
const pdConfig = {
  binFolder: manifest.wrapper.binFolder,
  pdFlags: manifest.wrapper.pdFlags,
  tmpDir: path.join(__dirname, `${manifest.wrapper.tmpDir}`),
  pdsendPort: manifest.wrapper.pdsendPort,
  pdreceivePort: manifest.wrapper.pdreceivePort
};

Pd.init(pdConfig).then(() => {
  // Log out when you're in there
  console.log(colors.green('ready!'));

  // Write something
  // to be delayed here
  setTimeout(() => {
    Pd.open(path.join(__dirname, 'main'))
    Pd.send('pd', 'dsp 1');
    console.log(colors.green('pd dsp is on'));
  }, 1000);

  // What to do on CTRL-C ?
  process.on( 'SIGINT', function() {
    console.info(colors.red('Received CTRL-C, stopping.'));
    Pd.clear();
    process.exit();
  });
});
